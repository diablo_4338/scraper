FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /site/src
WORKDIR /site

COPY requirements.txt /site/
RUN pip install -r requirements.txt

COPY docker-entrypoint.sh /site/

WORKDIR /site/src

CMD ["bash", "../docker-entrypoint.sh"]