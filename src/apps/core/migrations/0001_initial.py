# Generated by Django 3.0.2 on 2020-01-26 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок новости')),
                ('url', models.URLField(unique=True, verbose_name='URL новости')),
                ('created', models.DateTimeField(verbose_name='Время загрузки новости')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
    ]
