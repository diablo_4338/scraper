from rest_framework.generics import ListAPIView
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.filters import OrderingFilter

from apps.core.models import Post
from apps.core.serializers import PostSerializer
from apps.core.utils import get_posts_from_source


class PostListView(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [OrderingFilter]

    def get(self, request, *args, **kwargs):
        if 'update' in request.query_params:
            get_posts_from_source()
        return super(PostListView, self).get(request, *args, **kwargs)
