from django.contrib import admin

from apps.core.models import Post

admin.site.register(Post)
