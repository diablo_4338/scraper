from celery_config import app

from apps.core.utils import get_posts_from_source


@app.task(time_limit=30, soft_time_limit=10, max_retries=5, default_retry_delay=5)
def task_get_new_posts_from_source():
    get_posts_from_source()
