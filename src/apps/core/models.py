from django.db import models
from django.utils import timezone


class Post(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заголовок новости')
    url = models.URLField(verbose_name='URL новости', unique=True)
    created = models.DateTimeField(verbose_name='Время загрузки новости')

    class Meta:
        ordering = ['created']

    def __str__(self):
        return f'{self.id} - {self.title}'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.created = timezone.now()
        super(Post, self).save(force_insert, force_update, using,
                               update_fields)
