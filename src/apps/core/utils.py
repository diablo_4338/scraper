import requests

from bs4 import BeautifulSoup

from apps.core.models import Post


def save_post_to_db(title, url):
    if not url:
        return
    try:
        post = Post.objects.get(url=url)
    except Post.DoesNotExist:
        post = Post(url=url, title=title)
        post.save()
    return post


def get_posts_from_source():
    url = 'https://news.ycombinator.com/'
    response = requests.get(url=url)
    if response.status_code != 200:
        return
    bs = BeautifulSoup(response.content, features='html.parser', from_encoding=response.encoding)
    for element in bs.find_all('a', {'class': 'storylink'}):
        save_post_to_db(element.text, element.attrs.get('href'))
