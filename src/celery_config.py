import os

from celery import Celery


class Config(object):
    broker_url = 'redis://redis:6379/'
    timezone = 'Europe/Moscow'


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

app = Celery('scraper')

app.config_from_object(Config)

app.autodiscover_tasks()

app.conf.beat_schedule = {
    'get_new_posts_from_source': {
        'task': 'apps.core.tasks.task_get_new_posts_from_source',
        'schedule': 3600.0
    }
}
